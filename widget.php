<?php
	include_once 'util_db.php';
	include_once 'util_error.php';
	include_once 'util_file.php';
	include_once 'util_misc.php';
	include_once 'widget_modal.php';
	include_once 'widget_tile.php';
	include_once 'widget_nav.php';

	if(isset($_POST['logout'])){
 		session_unset(oid);
 		session_destroy(oid);
 		header("location:index.php");
 		exit;
 	}
?>

<?php
 	function init_ajax(){ ?>
      	<script src="3rd_party/js/jquery-3.3.1.min.js"></script>
      	<meta charset=utf-8 />
<?php	
	} ?>