<?php
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	include_once 'widget.php';
	init_ajax();

	$user_id;
	$user;
	if(isset($_GET['user_id']))
		$user_id = $_GET['user_id'];
	else{
		try{
			$_SESSION['user_id'];
			$user_id = $_SESSION['user_id'];
		}
		catch(Exception $e){
			header('location:index.php');
		}
	}
	$user = getUser($user_id);

	if(isset($_SESSION['edit_redirect'])){
		$id = $_SESSION['edit_redirect'];
		unset($_SESSION['edit_redirect']);
		header('location:page_property.php?prop_id='.$id);
	}

	if(isset($_POST['add']))
		header("location:page_property_add.php");
?>
 <!DOCTYPE HTML>
<html>
	<head>
		<title>Urban - Profile</title>
	</head>

	<body>
		<?php
			navBar();
		?>
		<br><br>
		<center>
		<div class='w3-card-2 w3-green' style='width: 260px; height: 250px;padding:16px; vertical-align: top;'>
			<div>
				<div class='w3-card-2' style='width:64px; height:64px; border:5px #5bcc62 solid;display: block ;margin-left: auto; margin-right: auto;  border-radius: 50%; background-size: cover;background-position: center; background-image: url("<?php echo getProfPicPath($user_id)?>")'></div>
				<h4 style="text-align: center"><?php echo $user->username ?></h4>
				<p style='margin-bottom: -5px'>Email</p>
				<?php echo $user->email ?>
				<p style='margin-bottom: -5px'>Phone</p>
				<?php echo $user->contact_no ?>
			</div>
		</div>
		</center>
		<br>
		<br>
		<?php
			group("PROPERTIES", getPropertyListByUser($user_id));
		?>
	</body>
</html>