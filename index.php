<?php
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	include_once 'widget.php';
	init_ajax();
	
	if(isset($_GET['logout']) && $_GET['logout']){
		session_unset();
		session_destroy();
		$_GET['logout'] = false;
		header('location:index.php');
	}
?>

<html>
	<head>
		<title>Urban - Log In </title>	
	</head>
	<body>
		<?php
			navBar();
			echo '<br><br>';
			groupV2("",getAllProperty());
		?>
	</body>
</html>