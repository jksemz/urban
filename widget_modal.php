<?php
  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);

 	function genericModal($id, $msg){ ?>
			<div id="<?php echo $id; ?>" class="w3-modal">
  				<div class="w3-modal-content w3-animate-zoom w3-card-4">
    				<div class="w3-container">
                <center>
      					  <p><?php echo $msg; ?></p>
                  <button class="w3-button w3-green" onclick="document.getElementById('<?php echo $id;?>').style.display='none'">OK</button>
                  <br><br>
                </center>
    				</div>
  				</div>
			</div>
<?php
 	} ?>

<?php
 	function genericModalShow($id){ ?>
			<script>document.getElementById('<?php echo $id; ?>').style.display="block"</script>
<?php
 	} ?>

<?php
  function loginModal(){ ?>
      <div id="loginModal" class="w3-modal">
          <div class="w3-modal-content w3-card-4 w3-animate-top" style='width: 500px'>
            <div class="w3-container">
                <span onclick="document.getElementById('loginModal').style.display='none'" 
                class="w3-button w3-display-topright">&times;</span>
                <h1>Log In</h1>
                <form method="POST">
                  <p>Email</p>
                  <input type='text' name='email' placeholder="Email">
                  <p>Password</p>
                  <input type='password' name='pass' placeholder="Password">
                  <br><br>
                  <button class='w3-button w3-green' name='loginbtn'>Log In</button>
                </form>
                <?php 
                    $fail = false;
                    if(isset($_POST['loginbtn'])){
                      $email = $_POST['email'];
                      $pass = $_POST['pass'];
                      $user = validateLogin($email, $pass);
                    
                      if($user != null){
                      #SUCCESS
                        $_SESSION['userid'] = $user->user_id;
                        header('location:page_profile.php');
                        exit;

                      } else{
                      #FAIL
                        $fail = true;
                      }
                    }
                ?>
            </div>
          </div>
      </div>
<?php
    if($fail){
      genericModal('loginError', 'Invalid Login');
      genericModalShow('loginError');
    }
  } ?>

<?php 
  function signupModal(){
      $fail = false;
      $error = "";
      $user = new User();
      $user->username = "";
      $user->password = "";
      $user->email = "";
      $user->contact_no = "";

      $prof = new Img();

      if(isset($_POST['sign'])){
          $user->username = $_POST['user'];
          $user->password = $_POST['pass'];
          $user->email = $_POST['email'];
          $user->contact_no = $_POST['contact'];

          $prof->name = $_FILES['ul_profpic']['name'];
          $prof->tmp_name = $_FILES['ul_profpic']['tmp_name'];
          $prof->size = $_FILES['ul_profpic']['size'];

          $error .= trapUser($user, $prof);

          if($error == ""){
            #SUCCESS
            createUser($user);
            $user = validateLogin($user->email, $user->password);

            #UPLOAD PHOTO
            ul_profpic($prof->name, $prof->tmp_name, $user->user_id);

            #PROCEED TO LOGIN
            $_SESSION['userid'] = $user->user_id;
            header('location:page_profile.php');
          } else {
            #FAIL
             genericModal('signupError', $error);
             genericModalShow('signupError');
          }
        }
?>
        <div id="signupModal" class="w3-modal w3-animate-fade">
          <div class="w3-modal-content w3-animate-top">
            <div class="w3-container w3-card-4">
                <span onclick="document.getElementById('signupModal').style.display='none'" 
                class="w3-button w3-display-topright">&times;</span>
                <h1>Sign Up</h1>
                <form method='POST' enctype='multipart/form-data'>
                <table>
                  <tr>
                    <td class='w3-padding-16'><h4 align='center'>Basics</h4></td>
                    <td rowspan="5" class='w3-padding-32' width="40px"></td>
                    <td colspan="2" class='w3-padding-16'><h4 align='center'>Profile Picture</h4></td>
                  </tr>
                  <tr>
                    <td><p>*User Name</p><input type='text' name='user'></td>
                    <td class='w3-display-top'rowspan="4">
                      <center>
                        <img id='prev_profpic' src='#'>
                        <br>
                        <input type='file' name='ul_profpic' accept='.jpg' onchange='profpic_uplprev(this)'>
                      </center>
                    </td>
                  </tr>
                  <tr>
                    <td><p>*Password</p><input type='password' name='pass'></td>                    
                  </tr>
                  <tr>
                    <td><p>*Email</p><input type='text' name='email'></td>
                  </tr>
                  <tr>
                    <td><p>*Contact No.</p><input type='text' name='contact'></td>              
                  </tr>
                </table>
                <p>* Required Field</p>
                <button class='w3-button w3-green' name='sign'>SIGN UP</button>
                <br><br>
                </form>
            </div>
          </div>
      </div>
      <script>
        function profpic_uplprev(file){
          if(file.files && file.files[0]){
            var img_prev = document.getElementById('prev_profpic');
            var reader = new FileReader();
            reader.onload = function (e) {
              $('#prev_profpic')
                .attr('src', e.target.result)
                .width(150)
                .height(150)
                .style('overflow','hidden');
            };
            reader.readAsDataURL(file.files[0]);
          }
      }
      </script>
<?php       
  }
?>
<?php
  function editUserModal(){
    $fail = false;
      $error = "";
      $user = getUser($_SESSION['user_id']);

      $prof = new Img();

      if(isset($_POST['e_sign'])){
          $user->username = $_POST['e_user'];
          $user->password = $_POST['e_pass'];
          $user->email = $_POST['e_email'];
          $user->contact_no = $_POST['e_contact'];

          if($_FILES['e_ul_profpic']['name'] != ''){
            $prof->name = $_FILES['e_ul_profpic']['name'];
            $prof->tmp_name = $_FILES['e_ul_profpic']['tmp_name'];
            $prof->size = $_FILES['e_ul_profpic']['size'];
          } else {
            $prof->name = 'blank';
            $prof->tmp_name = 'blank';
            $prof->size = 1;
            $prof->old = true;
          }

          $error .= trapUser($user, $prof);
          if($error == ""){
            #SUCCESS
            updateUser($user);
            #UPLOAD PHOTO
            if(!$prof->old)
              update_profpic($user->user_id, $prof);
?>        <script>window.location = 'page_profile.php';</script>
<?php     } else {
            #FAIL
             genericModal('e_signupError', $error);
             genericModalShow('e_signupError');
          }
      } else if(isset($_POST['e_del'])){
          deleteUser($user->user_id);
?>        <script>window.location='index.php?logout=true'</script>
<?php      
      }
?>
        <div id="editUserModal" class="w3-modal w3-animate-fade">
          <div class="w3-modal-content w3-animate-top">
            <div class="w3-container w3-card-4">
                <span onclick="document.getElementById('editUserModal').style.display='none'" 
                class="w3-button w3-display-topright">&times;</span>
                <h1>Edit User</h1>
                <form method='POST' enctype='multipart/form-data'>
                <table>
                  <tr>
                    <td class='w3-padding-16'><h4 align='center'>Basics</h4></td>
                    <td rowspan="5" class='w3-padding-32' width="40px"></td>
                    <td colspan="2" class='w3-padding-16'><h4 align='center'>Profile Picture</h4></td>
                  </tr>
                  <tr>
                    <td><p>*User Name</p><input type='text' name='e_user'></td>
                    <td class='w3-display-top'rowspan="4">
                      <center>
                        <img id='e_prev_profpic' src='<?php echo getProfPicPath($user->user_id) ?>' width='100px' height='100px'>
                        <br>
                        <input type='file' name='e_ul_profpic' accept='.jpg' onchange='e_profpic_uplprev(this)'>
                      </center>
                    </td>
                  </tr>
                  <tr>
                    <td><p>*Password</p><input type='password' name='e_pass'></td>                    
                  </tr>
                  <tr>
                    <td><p>*Email</p><input type='text' name='e_email'></td>
                  </tr>
                  <tr>
                    <td><p>*Contact No.</p><input type='text' name='e_contact'></td>              
                  </tr>
                </table>
                <p>* Required Field</p>
                <button class='w3-button w3-green' name='e_sign'>UPDATE</button>
                <button class='w3-button w3-red' name='e_del'>DELETE</button>
                <br><br>
                </form>
            </div>
          </div>
      </div>
      <script>
        function e_profpic_uplprev(file){
          if(file.files && file.files[0]){
            var reader = new FileReader();
            reader.onload = function (e) {
              $('#e_prev_profpic')
                .attr('src', e.target.result)
                .width(150)
                .height(150)
                .style('overflow','hidden');
            };
            reader.readAsDataURL(file.files[0]);
          } else {
            var img_prev = document.getElementById('e_prev_profpic');
            img_prev.src = '<?php echo getProfPicPath($user->user_id) ?>';
          }
      }
      </script>
      <script>
        document.getElementsByName('e_user')[0].value = '<?php echo $user->username; ?>';
        document.getElementsByName('e_pass')[0].value = '<?php echo $user->password; ?>';
        document.getElementsByName('e_email')[0].value = '<?php echo $user->email; ?>';
        document.getElementsByName('e_contact')[0].value = '<?php echo $user->contact_no; ?>';
      </script>
<?php
  }
?>
<?php
  function addPropModal(){
    $error = '';
    $propTypeList = getPropType();
    $transacTypeList = getTransacType();
    $rentIntvList = getRentIntv();

    if(isset($_POST['addProp'])){
      $prop = new Property();
      
      $prop->user_id = '';
      $prop->prop_type = '';
      $prop->prop_adType = '';
      $prop->addr_id = '';
      $prop->cover_pic_index = '';
      $prop->prop_title = ''; 
      $prop->prop_feat = '';
      $prop->prop_cost = 0;
      $prop->prop_rentIntv = '';
      $prop->prop_yrsToPay = 0;

      $prop->user_id = $_SESSION['user_id'];
      $prop->prop_type = $_POST['propType'];
      $prop->prop_adType = $_POST['adType'];
      $prop->prop_title = $_POST['prop_title'];
      $prop->prop_feat = strip_tags(trim(htmlspecialchars($_POST['prop_feat'])));
      
      
      switch($prop->prop_adType){
        case 'A':
          $prop->prop_cost = $_POST['prop_cost1'];
          break;
        case 'B':
          $prop->prop_cost = $_POST['prop_cost3'];
          $prop->prop_yrsToPay = $_POST['yrsToPay'];
          break;
        case 'C':
          $prop->prop_cost = $_POST['prop_cost2'];
          $prop->prop_rentIntv = $_POST['rentIntv'];
          break;
      }

      $ul_propPic = array();
      for($x=0; $x < 5; $x++){
          if($_FILES['ul_propPic'.$x]['name'] != ''){
            $propPic = new Img();
            $propPic->name = $_FILES['ul_propPic'.$x]['name'];
            $propPic->tmp_name = $_FILES['ul_propPic'.$x]['tmp_name'];
            $propPic->size = $_FILES['ul_propPic'.$x]['size'];
            array_push($ul_propPic, $propPic);
          }
      }

      if(count($ul_propPic) != 0){
        if(isset($_POST['propCover'])){
          $cover_tmp_index = $_POST['propCover'];
          for($x=0; $x < count($ul_propPic); $x++){
            if($ul_propPic[$x]->name == $_FILES['ul_propPic'.$cover_tmp_index]['name'])
              $prop->cover_pic_index = $x;
          }
        }
      }

      $addr = new Address();
      $addr->addr_id = '';
      $addr->addr_no = '';
      $addr->addr_st = '';
      $addr->addr_brgy = '';
      $addr->addr_city = '';
      $addr->addr_prov = '';
      $addr->addr_count = '';
      $addr->zip_code = '';

      $addr->addr_no = $_POST['addr_num'];
      $addr->addr_st = $_POST['addr_st'];
      $addr->addr_brgy = $_POST['addr_brgy'];
      $addr->addr_city = $_POST['addr_city'];
      $addr->addr_prov = $_POST['addr_prov'];
      $addr->addr_count = $_POST['addr_count'];
      $addr->zip_code = $_POST['zip_code'];

      $error .= trapProp($prop, $ul_propPic);
      $error .= trapAddr($addr);

      if($error == ''){
        $prop->addr_id = addAddress($addr);
        $prop->prop_id = addProperty($prop);
        ul_propPic($prop->prop_id, $ul_propPic);
        genericModal('PropSuccess', 'Property Posted');
        genericModalShow('PropSuccess');
      } else{
        genericModal('addPropError', $error);
        genericModalShow('addPropError');
      }
    }
?>
    <div id="addPropModal" class="w3-modal">
          <div class="w3-modal-content w3-card-4 w3-animate-top">
            <div class="w3-container">
            <!-- STEP 1 -->
            <form method="POST" enctype='multipart/form-data'>
              <div id='addprop1'>
                <span onclick="document.getElementById('addPropModal').style.display='none'" 
                class="w3-button w3-display-topright">&times;</span>
                <h1>Advertise Property</h1>
                  <table border='0'>
                    <tr>
                      <td colspan="3">
                        <h4>Basics</h4>
                      </td>
                      <td rowspan="6" width="50px"></td>
                      <td>
                        <h4>Upload Photo</h4>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>Title</p>
                        <input type='text' name='prop_title'>
                      </td>
                      <td rowspan="5" width="25px"></td>
                      <td rowspan="5">
                        <p>Ad Type</p>
                        <select name='adType' onchange='changeAdType(this)'>;
                        <?php foreach ($transacTypeList as $item) { ?>
                          <option value='<?php echo $item->transac_code ?>'><?php echo $item->desc ?></option>
                        <?php
                              }
                        ?>
                        </select>
                        <div>
                          <div id='sale'>
                            <p>Cost</p>
                            <input type='number' id='prop_cost1' name='prop_cost1'>
                          </div> 
                          <div id='rent'>
                            <p>Cost</p>
                            <input type='number' id='prop_cost2' name='prop_cost2'>
                            <p>Interval</p>
                            <?php foreach($rentIntvList as $item){ ?>
                              <input type='radio' name='rentIntv' value='<?php echo $item->rent_intv_code?>'>
                            <?php
                              echo $item->desc;
                              echo '<br>';
                            }
                            ?>
                          </div>
                          <div id='rentToOwn'>
                            <p>Cost</p>
                            <input type='number' id='prop_cost3' name='prop_cost3' onchange="monthlyPay()">
                            <p>Years to Pay</p>
                            <input type='number' id='yrsToPay' name='yrsToPay' onchange="monthlyPay()">
                            <p align="center">Installment</p>
                            <p class='w3-green w3-padding-16' id='monthPay' align="center">PHP 0.00/mo.</p>
                          </div>
                        </div>
                      </td>
                      <td rowspan="3">
                        <p>(Max 5 Photos, 2MB Max size per photo)</p>
                        <div style='height:300px;overflow:scroll;'>
<?php                    for($x=0; $x < 5; $x++){ ?>
                          <div class='w3-card-2' style='display:inline-block; white-space:nowrap'>
                            <img id='ul_propPrev<?php echo $x ?>' src='#' alt='Pic_<?php echo $x ?>' height='50px' width='50px'>
                            <input type='file' id='ul_propPic<?php echo $x ?>' name='ul_propPic<?php echo $x ?>' accept='.jpg'onchange='propPic_uplprev(this, <?php echo $x ?>)'>
                            <div><input type='radio' id='propCover<?php echo $x ?>' name='propCover' value='<?php echo $x ?>' disabled>Cover Photo</div>
                          </div>
                          <br>
<?php                   } ?>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>Type</p>
                        <select name='propType'>;
                        <?php foreach ($propTypeList as $item) { ?>
                          <option value='<?php echo $item->prop_typeCode ?>'><?php echo $item->desc ?></option>
                        <?php
                        }
                        ?>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td rowspan="3">
                        <p>Features</p>
                        <textarea name='prop_feat' rows="7" cols="20"></textarea>
                      </td>
                    </tr>
                  </table>
                   
                </div>
                <!-- STEP 2 -->
                <div id='addprop2'>
                <span onclick="document.getElementById('addPropModal').style.display='none'" 
                class="w3-button w3-display-topright">&times;</span>
                <h1>Advertise Property</h1>
                  <table border='0'>
                    <tr>
                      <td>
                        <h4>Address</h4>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>Street Number</p>
                        <input type='text' name='addr_num'>
                      </td>
                      <td>
                        <p>Province</p>
                        <input type='text' name='addr_prov'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>Street</p>
                        <input type='text' name='addr_st'>
                      </td>
                      <td>
                        <p>Country</p>
                        <input type='text' name='addr_count'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>Barangay/Municipality</p>
                        <input type='text' name='addr_brgy'>
                      </td>
                      <td>
                        <p>Zip Code</p>
                        <input type='text' name='zip_code'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>City</p>
                        <input type='text' name='addr_city'>
                      </td>
                      <td>
                        <button class='w3-button w3-green' name='addProp'>POST</button>
                      </td>
                    </tr>
                  </table>
                  </form>
                </div>          
                <br>
                <button id='propNxt' class='w3-button w3-green' onclick='
                if(pageNo == 1){pageNo = 2;changePage(pageNo, this);} 
                else{pageNo = 1;changePage(pageNo, this);}'>NEXT</button>
                <br><br>
              </div>
            </div>
          </div>
      </div>
       <script>
          document.getElementById('rent').style.display = 'none';
          document.getElementById('rentToOwn').style.display = 'none';
          function changeAdType(drop){
            if(drop.value == 'C'){
              document.getElementById('rent').style.display = 'block';
              document.getElementById('rentToOwn').style.display = 'none';
              document.getElementById('sale').style.display = 'none';
            }
            else if(drop.value == 'B'){
              document.getElementById('rent').style.display = 'none';
              document.getElementById('rentToOwn').style.display = 'block';
              document.getElementById('sale').style.display = 'none';
            }
            else{
              document.getElementById('rent').style.display = 'none';
              document.getElementById('rentToOwn').style.display = 'none';
              document.getElementById('sale').style.display = 'block';
            }
          }

          function monthlyPay(){
            var yrsToPay = document.getElementById('yrsToPay');
            var prop_cost = document.getElementById('prop_cost3');
            var monthPay = document.getElementById('monthPay');
            monthPay.innerHTML = "PHP "+Math.round(prop_cost.value / (yrsToPay.value * 12)) + "/mo.";
          }
      </script>
      <script>
        var pageNo = 1;
        var ulPropCount = 0;
        changePage(pageNo, document.getElementById('propNxt'));
        function changePage(index, button){
          for(var x=0; x < 2; x++){
            var page = document.getElementById('addprop'+(x+1));
            page.style.display = 'none';
          }
          document.getElementById('addprop'+index).style.display = 'block';

          if(pageNo == 2)
            button.innerHTML = 'PREV';
          else 
            button.innerHTML = 'NEXT';
        }

        function propPic_uplprev(file, index){
          var reader = new FileReader();
          if(file.files && file.files[0]){
            document.getElementById('propCover'+index).disabled = false;
            reader.onload = function (e) {
              $('#ul_propPrev'+index)
                .attr('src', e.target.result)
                .width(50)
                .height(50)
                .style('overflow','hidden');
            };
            reader.readAsDataURL(file.files[0]);
          } else{
            var coverPicRadios = document.getElementsByName('propCover');
            for($x=0; $x < coverPicRadios.length; $x++){
              coverPicRadios[$x].checked = false;
            }
            document.getElementById('propCover'+index).disabled = true;
            document.getElementById('ul_propPrev'+index).src = '#.jpg';
          }
        }
      </script>
<?php    
  }
?>

<?php
  function editPropModal($prop_id){
    $error = '';
    $prop = getProperty($prop_id);
    $addr = getAddress($prop->addr_id);
    
    # Existing Photos
    $curr_propPic = getPropPicPath($prop_id);
    # To be Deleted Photos
    $del_propPic = array();
    # New Photos
    $new_propPic = array();
    # Temp Merged 
    $merge_propPic = array();

    $propTypeList = getPropType();
    $transacTypeList = getTransacType();
    $rentIntvList = getRentIntv();

    if(isset($_POST['e_addProp'])){
/*
      $prop = new Property();
      
      $prop->user_id = '';
      $prop->prop_type = '';
      $prop->prop_adType = '';
      $prop->addr_id = '';
      $prop->cover_pic_index = '';
      $prop->prop_title = ''; 
      $prop->prop_feat = '';
      $prop->prop_cost = 0;
      $prop->prop_rentIntv = '';
      $prop->prop_yrsToPay = 0; */

      $prop->prop_type = $_POST['e_propType'];
      $prop->prop_adType = $_POST['e_adType'];
      $prop->prop_title = $_POST['e_prop_title'];
      $prop->prop_feat = strip_tags(trim(htmlspecialchars($_POST['e_prop_feat'])));
       
      switch($prop->prop_adType){
        case 'A':
          $prop->prop_cost = $_POST['e_prop_cost1'];
          break;
        case 'B':
          $prop->prop_cost = $_POST['e_prop_cost3'];
          $prop->prop_yrsToPay = $_POST['e_yrsToPay'];
          break;
        case 'C':
          $prop->prop_cost = $_POST['e_prop_cost2'];
          $prop->prop_rentIntv = $_POST['e_rentIntv'];
          break;
      }    
      
      #Transfer new pics to new list
      for($x=count($curr_propPic); $x < 5; $x++){
          if($_FILES['e_ul_propPic'.$x]['name'] != ''){
            $propPic = new Img();
            $propPic->name = $_FILES['e_ul_propPic'.$x]['name'];
            $propPic->tmp_name = $_FILES['e_ul_propPic'.$x]['tmp_name'];
            $propPic->size = $_FILES['e_ul_propPic'.$x]['size'];
            array_push($new_propPic, $propPic);
          }
      }

      $cover_tmp_path = '';
      $cover_tmp_index = '';
      if(isset($_POST['e_propCover'])){
        if($_POST['e_propCover'] < count($curr_propPic))
          $cover_tmp_path = $curr_propPic[$_POST['e_propCover']];
        else
          $cover_tmp_path = $_FILES['e_ul_propPic'.$_POST['e_propCover']]['name'];
      }
      #print_r(array_values($new_propPic));
      #print_r(array_values($curr_propPic));
      #Transfer delete pics to delete list
      if(isset($_POST['e_propPicDelete'])){
        foreach ($_POST['e_propPicDelete'] as $del_index) {
          array_push($del_propPic, $curr_propPic[$del_index]);
          unset($curr_propPic[$del_index]);
        }
        $curr_propPic = array_values($curr_propPic);
      }

      #print_r(array_values($curr_propPic));
      #print_r(array_values($del_propPic));

      #Merge Old & New Pics for traping & cover photo set
      for($x=0; $x < count($curr_propPic); $x++){
        $propPic = new Img();
        $propPic->name = $curr_propPic[$x];
        $propPic->old = true;
        array_push($merge_propPic, $propPic);
      }

      for($x=0; $x < count($new_propPic); $x++)
        array_push($merge_propPic, $new_propPic[$x]);

      if($cover_tmp_path != ''){
        foreach($merge_propPic as $x => $item){
          if($item->name == $cover_tmp_path)
            $prop->cover_pic_index = $x;
        }
      } else {
        $prop->cover_pic_index = '';
      }
      #print_r(array_values($merge_propPic));
      
      /*
      */
/*
      $addr = new Address();
      $addr->addr_id = '';
      $addr->addr_no = '';
      $addr->addr_st = '';
      $addr->addr_brgy = '';
      $addr->addr_city = '';
      $addr->addr_prov = '';
      $addr->addr_count = '';
      $addr->zip_code = ''; */

      $addr->addr_no = $_POST['e_addr_num'];
      $addr->addr_st = $_POST['e_addr_st'];
      $addr->addr_brgy = $_POST['e_addr_brgy'];
      $addr->addr_city = $_POST['e_addr_city'];
      $addr->addr_prov = $_POST['e_addr_prov'];
      $addr->addr_count = $_POST['e_addr_count'];
      $addr->zip_code = $_POST['e_zip_code'];

      $error .= trapProp($prop, $merge_propPic);
      $error .= trapAddr($addr);

      if($error == ''){
        #$prop->addr_id = addAddress($addr);
        #$prop->prop_id = addProperty($prop);
        update_propPic($prop_id, $merge_propPic, $del_propPic);
        updateProperty($prop);
        updateAddress($addr);
        genericModal('e_PropSuccess', 'Property Updated <br> Cover '.$prop->cover_pic_index);
        genericModalShow('e_PropSuccess');
        $_SESSION['edit_redirect'] = $prop_id;
?>      <script>window.location = 'page_profile.php';</script>
<?php
        #print_r($prop);
      } else{
        genericModal('e_addPropError', $error."<br>".$cover_tmp_path);
        genericModalShow('e_addPropError');
      }
    } else if (isset($_POST['e_delProp'])){
      deleteProperty($prop->prop_id);
      deleteAddress($prop->addr_id);
      delete_propPic($prop->prop_id);
?>    <script>window.location = 'page_profile.php';</script>
<?php
    }
?>
    <div id="editPropModal" class="w3-modal">
          <div class="w3-modal-content w3-card-4 w3-animate-top">
            <div class="w3-container">
            <!-- STEP 1 -->
            <form method="POST" enctype='multipart/form-data'>
              <div id='e_addprop1'>
                <span onclick="document.getElementById('editPropModal').style.display='none'" 
                class="w3-button w3-display-topright">&times;</span>
                <h1>Edit Property</h1>
                  <table border='0'>
                    <tr>
                      <td colspan="3">
                        <h4>Basics</h4>
                      </td>
                      <td rowspan="6" width="50px"></td>
                      <td>
                        <h4>Upload Photo</h4>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>Title</p>
                        <input type='text' name='e_prop_title'>
                      </td>
                      <td rowspan="5" width="25px"></td>
                      <td rowspan="5">
                        <p>Ad Type</p>
                        <select name='e_adType' onchange='e_changeAdType(this)'>;
                        <?php foreach ($transacTypeList as $item) { ?>
                          <option value='<?php echo $item->transac_code ?>'><?php echo $item->desc ?></option>
                        <?php
                              }
                        ?>
                        </select>
                        <div>
                          <div id='e_sale'>
                            <p>Cost</p>
                            <input type='number' id='e_prop_cost1' name='e_prop_cost1'>
                          </div> 
                          <div id='e_rent'>
                            <p>Cost</p>
                            <input type='number' id='e_prop_cost2' name='e_prop_cost2'>
                            <p>Interval</p>
                            <?php foreach($rentIntvList as $item){ ?>
                              <input type='radio' name='e_rentIntv' value='<?php echo $item->rent_intv_code?>'>
                            <?php
                              echo $item->desc;
                              echo '<br>';
                            }
                            ?>
                          </div>
                          <div id='e_rentToOwn'>
                            <p>Cost</p>
                            <input type='number' id='e_prop_cost3' name='e_prop_cost3' onchange="e_monthlyPay()">
                            <p>Years to Pay</p>
                            <input type='number' id='e_yrsToPay' name='e_yrsToPay' onchange="e_monthlyPay()">
                            <p align="center">Installment</p>
                            <p class='w3-green w3-padding-16' id='e_monthPay' align="center">PHP 0.00/mo.</p>
                          </div>
                        </div>
                      </td>
                      <td rowspan="3">
                        <p>(Max 5 Photos, 2MB Max size per photo)</p>
                        <div style='height:300px;overflow:scroll;'>
<?php                    for($x=0; $x < count($curr_propPic); $x++){ ?>
                          <div class='w3-card-2' style='display:inline-block;width:357px'>
                            <img id='e_ul_propPrev<?php echo $x ?>' src='<?php echo $curr_propPic[$x] ?>' alt='Pic_<?php echo $x ?>' height='50px' width='50px'>
                            <input type='checkbox' id='e_propPicDelete<?php echo $x ?>' name='e_propPicDelete[]' value='<?php echo $x ?>' 
                            onclick='
                            if(document.getElementsByName("e_propCover")[this.value].checked)
                              e_clearRadios();
                            document.getElementsByName("e_propCover")[this.value].disabled = this.checked
                            '>Delete
                            <div><input type='radio' id='e_propCover<?php echo $x ?>' name='e_propCover' value='<?php echo $x ?>'>Cover Photo</div>
                          </div>
                          <br>
<?php                     } 
                         for($x=count($curr_propPic); $x < 5; $x++){ ?>
                            <div class='w3-card-2' style='display:inline-block; white-space:nowrap'>
                            <img id='e_ul_propPrev<?php echo $x ?>' src='#' alt='Pic_<?php echo $x ?>' height='50px' width='50px'>
                            <input type='file' id='e_ul_propPic<?php echo $x ?>' name='e_ul_propPic<?php echo $x ?>' accept='.jpg'onchange='e_propPic_uplprev(this, <?php echo $x ?>)'>
                            <div><input type='radio' id='e_propCover<?php echo $x ?>' name='e_propCover' value='<?php echo $x ?>' disabled>Cover Photo <?php echo $x ?></div>
                          </div>
                          <br>
<?php                     } ?>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>Type</p>
                        <select name='e_propType'>;
                        <?php foreach ($propTypeList as $item) { ?>
                          <option value='<?php echo $item->prop_typeCode ?>'><?php echo $item->desc ?></option>
                        <?php
                        }
                        ?>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td rowspan="3">
                        <p>Features</p>
                        <textarea name='e_prop_feat' rows="7" cols="20"></textarea>
                      </td>
                    </tr>
                  </table>
                   
                </div>
                <!-- STEP 2 -->
                <div id='e_addprop2'>
                <span onclick="document.getElementById('editPropModal').style.display='none'" 
                class="w3-button w3-display-topright">&times;</span>
                <h1>Edit Property</h1>
                  <table border='0'>
                    <tr>
                      <td>
                        <h4>Address</h4>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>Street Number</p>
                        <input type='text' name='e_addr_num'>
                      </td>
                      <td>
                        <p>Province</p>
                        <input type='text' name='e_addr_prov'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>Street</p>
                        <input type='text' name='e_addr_st'>
                      </td>
                      <td>
                        <p>Country</p>
                        <input type='text' name='e_addr_count'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>Barangay/Municipality</p>
                        <input type='text' name='e_addr_brgy'>
                      </td>
                      <td>
                        <p>Zip Code</p>
                        <input type='text' name='e_zip_code'>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>City</p>
                        <input type='text' name='e_addr_city'>
                      </td>
                      <td>
                        <button class='w3-button w3-green' name='e_addProp'>POST</button>
                        <button class='w3-button w3-red' name='e_delProp'>DELETE</button>
                      </td>
                    </tr>
                  </table>
                  </form>
                </div>          
                <br>
                <button id='e_propNxt' class='w3-button w3-green' onclick='
                if(e_pageNo == 1){e_pageNo = 2;e_changePage(e_pageNo, this);} 
                else{e_pageNo = 1;e_changePage(e_pageNo, this);}'>NEXT</button>
                <br><br>
              </div>
            </div>
          </div>
      </div>
       <script>
          document.getElementById('e_rent').style.display = 'none';
          document.getElementById('e_rentToOwn').style.display = 'none';
          function e_changeAdType(drop){
            if(drop.value == 'C'){
              document.getElementById('e_rent').style.display = 'block';
              document.getElementById('e_rentToOwn').style.display = 'none';
              document.getElementById('e_sale').style.display = 'none';
            }
            else if(drop.value == 'B'){
              document.getElementById('e_rent').style.display = 'none';
              document.getElementById('e_rentToOwn').style.display = 'block';
              document.getElementById('e_sale').style.display = 'none';
            }
            else{
              document.getElementById('e_rent').style.display = 'none';
              document.getElementById('e_rentToOwn').style.display = 'none';
              document.getElementById('e_sale').style.display = 'block';
            }
          }

          function e_monthlyPay(){
            var yrsToPay = document.getElementById('e_yrsToPay');
            var prop_cost = document.getElementById('e_prop_cost3');
            var monthPay = document.getElementById('e_monthPay');
            monthPay.innerHTML = "PHP "+Math.round(prop_cost.value / (yrsToPay.value * 12)) + "/mo.";
          }
      </script>
      <script>
        var e_pageNo = 1;
        var e_ulPropCount = 0;
        e_changePage(e_pageNo, document.getElementById('e_propNxt'));
        function e_changePage(index, button){
           for(var x=0; x < 2; x++){
            var e_page = document.getElementById('e_addprop'+(x+1));
            e_page.style.display = 'none';
          }
          document.getElementById('e_addprop'+index).style.display = 'block';

          if(e_pageNo == 2)
            button.innerHTML = 'PREV';
          else 
            button.innerHTML = 'NEXT';
        }

        function e_propPic_uplprev(file, index){
          var reader = new FileReader();
          if(file.files && file.files[0]){
            document.getElementById('e_propCover'+index).disabled = false;
            reader.onload = function (e) {
              $('#e_ul_propPrev'+index)
                .attr('src', e.target.result)
                .width(50)
                .height(50)
                .style('overflow','hidden');
            };
            reader.readAsDataURL(file.files[0]);
          } else{
            clearRadios();
            document.getElementById('e_propCover'+index).disabled = true;
            document.getElementById('e_ul_propPrev'+index).src = '#.jpg';
          }
        }

        function e_clearRadios(){
          var coverPicRadios = document.getElementsByName('e_propCover');
            for($x=0; $x < coverPicRadios.length; $x++){
              coverPicRadios[$x].checked = false;
            }
        }
      </script>
      <script>
        document.getElementsByName('e_prop_title')[0].value = '<?php echo $prop->prop_title ?>';
        document.getElementsByName('e_propType')[0].value = '<?php echo $prop->prop_type ?>';
        var temp_feat = '<?php echo preg_replace("#\r\n+#", "<x>", $prop->prop_feat) ?>';
        document.getElementsByName('e_prop_feat')[0].value = temp_feat.replace(new RegExp('<x>','g'), String.fromCharCode(13,10));
        var adType = '<?php echo $prop->prop_adType ?>';
        document.getElementsByName('e_adType')[0].value = adType;
        e_changeAdType(document.getElementsByName('e_adType')[0]);
        switch(adType){
          case 'A':
            document.getElementById('e_prop_cost1').value = <?php echo $prop->prop_cost ?>;
            break;
          case 'B':
            document.getElementById('e_yrsToPay').value = <?php echo $prop->prop_yrsToPay ?>;
            document.getElementById('e_prop_cost3').value = <?php echo $prop->prop_cost ?>;
            e_monthlyPay();
            break;
          case 'C':
            document.getElementById('e_prop_cost2').value = <?php echo $prop->prop_cost ?>;
            switch('<?php echo $prop->prop_rentIntv ?>'){
              case 'A':
                document.getElementsByName('e_rentIntv')[0].checked = true;
                break;
              case 'B':
                document.getElementsByName('e_rentIntv')[1].checked = true;
                break;
              case 'C':
                document.getElementsByName('e_rentIntv')[2].checked = true;
                break;
              case 'D':
                document.getElementsByName('e_rentIntv')[3].checked = true;
                break;
            }
            break;
        }

        document.getElementsByName('e_propCover')[<?php echo $prop->cover_pic_index ?>].checked = true;
        document.getElementsByName('e_addr_num')[0].value = '<?php echo $addr->addr_no ?>';
        document.getElementsByName('e_addr_prov')[0].value = '<?php echo $addr->addr_prov ?>';
        document.getElementsByName('e_addr_st')[0].value = '<?php echo $addr->addr_st ?>';
        document.getElementsByName('e_addr_count')[0].value = '<?php echo $addr->addr_count ?>';
        document.getElementsByName('e_addr_brgy')[0].value = '<?php echo $addr->addr_brgy ?>';
        document.getElementsByName('e_zip_code')[0].value = '<?php echo $addr->zip_code ?>';
        document.getElementsByName('e_addr_city')[0].value = '<?php echo $addr->addr_city ?>';
      </script>
<?php    
  }
?>