<?php
	function trapUser($user, $profPic){
		$error = "";

		#Email Trapping
		if(getUser($user->user_id) != null){
			if(emailExists($user->email) && $user->email != getUser($user->user_id)->email)
				$error .= "Email Exists <br>";
		} else {
			if(emailExists($user->email))
				$error .= "Email Exists <br>";
		}

		#Photo Trapping
        if($profPic->name == '')
              $error .= "No Photo Uploaded <br>";
        if($profPic->size > 2000000 || $profPic->size == 0)
              $error .= "Photo must be less than 2MB <br>";
		
		#Length Trapping
		if(minLength(4,$user->username) || maxLength(50,$user->username))
			$error .= "Username 4-30 characters only <br>";
		if(minLength(6,$user->password) || maxLength(20,$user->password))
			$error .= "Password 6-15 characters only <br>";
		if(minLength(1,$user->email) || maxLength(20,$user->email))
			$error .= "Email 30 characters only <br>";
		if(minLength(1,$user->contact_no) || maxLength(20,$user->contact_no))
			$error .= "Contact Number 11 characters only <br>";
		
		return $error;
	}

	function trapAddr($addr){
		$error = "";
		#Length Trapping
		if(maxLength(5,$addr->addr_no))
			$error .= "Address Number 5 characters only <br>";
		if(minLength(1,$addr->addr_st) || maxLength(30,$addr->addr_st))
			$error .= "Street 30 characters only <br>";
		if(minLength(1,$addr->addr_brgy) || maxLength(20,$addr->addr_brgy))
			$error .= "Barangay/Municipality 20 characters only <br>";
		if(minLength(1,$addr->addr_prov) || maxLength(20,$addr->addr_prov))
			$error .= "Province/State 20 characters only <br>";
		if(minLength(1,$addr->addr_count) || maxLength(20,$addr->addr_count))
			$error .= "Country 20 characters only <br>";
		if(minLength(1,$addr->zip_code) || maxLength(5,$addr->zip_code))
			$error .= "Zip Code 30 characters only <br>";

		return $error;
	}

	function trapProp($prop, $propPic){
		$error = "";
		#Length Trapping
		if(minLength(1,$prop->prop_title) || maxLength(50,$prop->prop_title))
			$error .= "Property Title 50 characters only <br>";

		if(maxLength(512,$prop->prop_feat))
			$error .= "Features exceed 512 characters <br>";

		switch($prop->prop_adType){
        case 'A':
          if(minLength(1,$prop->prop_cost) || maxLength(12,$prop->prop_cost) || $prop->prop_cost== 0)
          	$error .= "Cost Error <br>";
          break;
        case 'B':
          if(minLength(1,$prop->prop_cost) || maxLength(12,$prop->prop_cost) || $prop->prop_cost == 0)
          	$error .= "Cost Error <br>";
          if(minLength(1,$prop->prop_yrsToPay) || maxLength(2,$prop->prop_yrsToPay) || $prop->prop_yrsToPay == 0)
          	$error .= "Year to Pay Error <br>";
          break;
        case 'C':
          if(minLength(1,$prop->prop_cost) || maxLength(12,$prop->prop_cost) || $prop->prop_cost == 0)
          	$error .= "Cost Error <br>";
          if($prop->prop_rentIntv == '')
          	$error .= "Rent Interval None Selected <br>";
          break;
      	}

		#Photo Trapping
		#Size limit
		for($x=0; $x < count($propPic); $x++){
			if(!$propPic[$x]->old){
				if($propPic[$x]->size > 2000000 || $propPic[$x]->size == 0)
					$error .= $propPic[$x]->name." exceed 2MB <br>";
			}
		}

		#Empty Upload
		if(count($propPic) == 0)
			$error .= "No Photos Uploaded <br>";

		#No cover photo
		if(!is_numeric($prop->cover_pic_index))
			$error .= "No Cover Photo Selected :".$prop->cover_pic_index."<br>";
		return $error;
	}

	function minLength($len, $str){
		return strlen($str) < $len;
	}

	function maxLength($len, $str){
		return strlen($str) > $len;
	}
?>