<?php
	class User{
		var $user_id;
		var $addr_id;
		var $username;
		var $password;
		var $email;
		var $contact_no;
		var $date_m;
		var $date_d;
		var $date_y;
	}

	class Address{
		var $addr_id;
		var $addr_no;
		var $addr_st;
		var $addr_brgy;
		var $addr_city;
		var $addr_prov;
		var $addr_count;
		var $zip_code;
	}

	class Property{
		var $prop_id;
		var $user_id;
		var $prop_type;
		var $prop_adType;
		var $addr_id;
		var $cover_pic_index;
		var $prop_title;
		var $prop_feat;
		var $prop_cost;
		var $prop_rentIntv;
		var $prop_yrsToPay;
		var $prop_acquired;
	}

	class PropertyType{
		var $prop_typeCode;
		var $desc;
	}

	class TransacType{
		var $transac_code;
		var $desc;
	}

	class RentIntv{
		var $rent_intv_code;
		var $desc;
	}

	class Img{
		var $name;
		var $tmp_name;
		var $size;
		var $old = false;
	}
?>