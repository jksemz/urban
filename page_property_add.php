<?php
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	include_once 'widget.php';

	$user_id = $_SESSION["userid"];
	$user = getUser($user_id);

	if(isset($_POST['addProp'])){	
		$item = new Property();
		$item->user_id = $user_id;
		$item->prop_title = $_POST['prop_title'];
		$item->prop_cost = $_POST['prop_cost'];
		$item->prop_typeCode = $_POST['propType'];
		$item->addr_id = 0;
		addProperty($item);
		header("location:page_profile.php");
	}

	
?>

<html>
	<head>
		<title>Urban - Profile</title>
	</head>

	<body>
		<?php
			navBar();
		?>

		<form method="POST">
			<p>Property Title</p>
			<input type='text' name='prop_title'>
			<p>Cost</p>
			<input type='number' name='prop_cost'>
			<p>Type</p>
			<?php propTypeDrop() ?>
			<br>
			<button name='addProp'>Add</button>
		</form>
	</body>
</html>