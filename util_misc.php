<?php
	#util_misc
	function shortenCost($cost){
		if($cost >= 1000000)
			return round($cost / 1000000,2).'M';
		else if($cost >= 1000)
			return round($cost / 1000,2).'K';
		else
			return $cost;
	}
?>