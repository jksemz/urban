<?php  
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	include_once 'util.php';
	include_once 'widget.php';

	$username = "";
	$pass = "";
	$email = "";
	if(isset($_POST['sign'])){
		$username = $_POST['user'];
		$pass = $_POST['pass'];
		$email = $_POST['email'];
		
		$user = new User();
		$user->username = $username;
		$user->password = $pass;
		$user->email = $email;
		$user->addr_id = 0;
		$user->contact_no = "";

		if($username != null && !validateUsername($user->username)){
			#SUCCESS
			echo "success!";
			createUser($user);
			header("location:index.php");
		} else{
			echo "fail!";
			#FAIL
		}
	}
?>

<html>
	<head>
		<title>Urban - Sign Up</title>
	</head>
	
	<body>
		<?php
			navTest();
		?>
		<h1>Sign Up</h1>
		<form method = POST>

		<!-- Username -->
		<p>User Name</p>
		<input type = 'text' name='user'>

		<!-- Password -->
		<p>Password</p>
		<input type = 'password' name='pass' placeholder="********">

		<!-- Email -->
		<p>Email</p>
		<input type = 'text' name='email'>
		
		<br>
		<button name='sign'>SIGN UP</button>
		</form>
	</body>
</html>