<?php
	function getRootDir(){
		return $_SERVER['DOCUMENT_ROOT'];
	}

	function cutRootDir($path){
		return substr($path, 41);
	}

	function getProfDir(){
		return getRootDir()."/img/prof/";
	}

	function getPropDir(){
		return getRootDir()."/img/prop/";
	}

	function ul_profPic($file, $file_tmp, $user_id){
		$dir = getProfDir();
		mkdir($dir.$user_id);
		$dir .= $user_id."/".$file;
		move_uploaded_file($file_tmp, $dir);
	}

	function update_profPic($user_id, $profPic){
		$dir = getProfDir().$user_id."/";
		delete_profPic($user_id);
		ul_profPic($profPic->name, $profPic->tmp_name, $user_id);
	}

	function delete_profPic($user_id){
		$dir = getProfDir().$user_id."/";

		$files = scandir($dir);
		unlink($dir.$files[2]);
		rmdir($dir);
	}

	function ul_propPic($prop_id, $propPic){
		$dir = getPropDir();
		mkdir($dir.$prop_id);
		$dir .= $prop_id."/";
		for($x=0; $x < count($propPic); $x++){
			move_uploaded_file($propPic[$x]->tmp_name, $dir.$propPic[$x]->name);
			rename($dir.$propPic[$x]->name, $dir.$x.".jpg");
		}	
	}

	function update_propPic($prop_id, $merge, $del){
		$temp_dir = getPropDir().'temp_'.$prop_id.'/';
		$dir = getPropDir().$prop_id.'/';
		
		# Delete Pics
		foreach ($del as $del_item)
			unlink(getRootDir().'/'.$del_item);

		mkdir($temp_dir);

		# Move Old pics temporarily
		for($x=0; $x < count($merge); $x++){
			if($merge[$x]->old)
				rename(getRootDir().'/'.$merge[$x]->name, $temp_dir.basename($merge[$x]->name));
		}

		rmdir($dir);
		mkdir($dir);

		# New & old pics moved to new folder
		for($x=0; $x < count($merge); $x++){
			if($merge[$x]->old)
				rename($temp_dir.basename($merge[$x]->name), $dir.$x.'.jpg');
			else{
				move_uploaded_file($merge[$x]->tmp_name, $dir.$merge[$x]->name);
				$merge[$x]->name = rename($dir.$merge[$x]->name, $dir.$x.".jpg");
			}
		}

		rmdir($temp_dir);
	}

	function delete_propPic($prop_id){
		$dir = getPropDir().$prop_id."/";
		$files = scandir($dir);
		foreach ($files as $x => $file) {
			if($x >= 2)
				unlink($dir.$file);
		}
		rmdir($dir);
	}

	function getProfPicPath($user_id){
		$dir = getProfDir().$user_id."/";
		$files = scandir($dir);
		return cutRootDir($dir.$files[2]);
	}

	function getPropPicPath($prop_id){
		$dir = getPropDir().$prop_id."/";
		$files = scandir($dir);

		$propPicPath = array();
		for($x=2; $x < count($files); $x++){
			if($files[$x] != '')
				array_push($propPicPath, cutRootDir($dir.$files[$x]));
		}
		return $propPicPath;
	}

	function getCoverPropPath($prop_id, $coverIndex){
		$dir = getPropDir().$prop_id."/";
		$files = scandir($dir);
		return cutRootDir($dir.$files[2+$coverIndex]);
	}
?>