<?php
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	include 'classes.php';

	function getConnection(){
		$server = "localhost";
		$user = "root";
		$pass = "uniteitall";
		$db = "urban";
		return new mysqli($server, $user, $pass, $db);
	}

	function getUser($user_id){
		$conn = getConnection();
		$user = new User();

		$query = "SELECT * FROM user WHERE user_id ='".$user_id."'";
		$result = mysqli_query($conn, $query);

		if(mysqli_num_rows($result) > 0){
			$row = mysqli_fetch_assoc($result);
			$user->user_id = $row['user_id'];
			$user->username = $row['username'];
			$user->password = $row['password'];
			$user->email = $row['email'];
			$user->contact_no = $row['contact_no'];
		} else
			$user = null;
		mysqli_close($conn);
		return $user;
	}

	function getUsername($user_id){
		$conn = getConnection();
		$query = "SELECT username FROM user WHERE user_id ='".$user_id."'";
		$result = mysqli_query($conn, $query);
		if(mysqli_num_rows($result) > 0){
			$row = mysqli_fetch_assoc($result);
			return $row['username'];
		}
		return "n/a";
	}

	function getUserContact($user_id){
		$conn = getConnection();
		$query = "SELECT email, contact_no FROM user WHERE user_id ='".$user_id."'";
		$result = mysqli_query($conn, $query);
		
		$user = new User();
		$user->email = 'n/a';
		$user->contact_no = 'n/a';
		if(mysqli_num_rows($result) > 0){
			$row = mysqli_fetch_assoc($result);
			$user->email = $row['email'];
			$user->contact_no = $row['contact_no'];
		}
		mysqli_close($conn);
		return $user;
	}

	function createUser($user){
		$conn = getConnection();
		$query = "INSERT INTO user (username, password, email, contact_no) VALUES (".
		"'".$user->username."',".
		"'".$user->password."',".
		"'".$user->email."',".
		"'".$user->contact_no."')";
		mysqli_query($conn, $query);
		mysqli_close($conn);
	}

	/*(username, password, email, contact_no)*/
	function updateUser($user){
		$conn = getConnection();
		$query = "UPDATE user SET ".
		"username = '".$user->username."',".
		"password = '".$user->password."',".
		"email = '".$user->email."',".
		"contact_no = '".$user->contact_no."' ".
		"WHERE user_id ='".$user->user_id."'";
		mysqli_query($conn, $query);
		mysqli_close($conn);
	}

	function deleteUser($user_id){
		$propList = getPropertyListByUser($user_id);
		foreach($propList as $item){
			deleteProperty($item->prop_id);
			deleteAddress($item->addr_id);
			delete_propPic($item->prop_id);
		}

		$conn = getConnection();
		$query = "DELETE FROM user WHERE user_id='".$user_id."'";
		mysqli_query($conn, $query);
		mysqli_close($conn);
		delete_profPic($user_id);
	}

	function deleteUserNoPhoto($user_id){
		$propList = getPropertyListByUser($user_id);
		foreach($propList as $item){
			deleteProperty($item->prop_id);
			deleteAddress($item->addr_id);
		}

		$conn = getConnection();
		$query = "DELETE FROM user WHERE user_id='".$user_id."'";
		mysqli_query($conn, $query);
		mysqli_close($conn);
		delete_profPic($user_id);
	}


	function validateLogin($email, $password){
		$conn = getConnection();
		$query = "SELECT * FROM user WHERE email ='".$email."' AND password ='".$password."'";
		$result = mysqli_query($conn, $query);
		if(mysqli_num_rows($result) > 0){
			$row = mysqli_fetch_assoc($result);
			$user = new User();
			$user->user_id = $row['user_id'];
			$user->addr_id = $row['addr_id'];
			$user->username = $row['username'];
			$user->email = $row['email'];
			$user->contact_no = $row['contact_no'];
			$_SESSION['user_id'] = $user->user_id;
			return $user;
		}
		else
			return null;
	}

	function emailExists($email){
		$conn = getConnection();
		$query = "SELECT email FROM user WHERE email ='".$email."'";
		$result = mysqli_query($conn, $query);
		if(mysqli_num_rows($result) > 0)
			return true;
		return false;
	}

	function getAddress($addr_id){
		$conn = getConnection();
		$addr = new Address();
		$query = "SELECT * FROM address WHERE addr_id ='".$addr_id."'";

		$result = mysqli_query($conn, $query);
		if(mysqli_num_rows($result) > 0){
			$row = mysqli_fetch_assoc($result);
			$addr->addr_id = $row['addr_id'];
			$addr->addr_no = $row['addr_no'];
			$addr->addr_st = $row['addr_street'];
			$addr->addr_brgy = $row['addr_brgy'];
			$addr->addr_city = $row['addr_city'];
			$addr->addr_prov= $row['addr_province'];
			$addr->addr_count = $row['addr_country'];
			$addr->zip_code = $row['postal_code'];
		} else
			$addr = null;
		return $addr;
	}

	function addAddress($addr){
		$conn = getConnection();
		$query = "INSERT INTO address(addr_no, addr_street, addr_brgy, addr_city, addr_province, addr_country, postal_code) VALUES(".
		"'".$addr->addr_no."',".
		"'".$addr->addr_st."',".
		"'".$addr->addr_brgy."',".
		"'".$addr->addr_city."',".
		"'".$addr->addr_prov."',".
		"'".$addr->addr_count."',".
		"'".$addr->zip_code."')";
		mysqli_query($conn, $query);

		$query = "SELECT last_insert_id()";
		$result = mysqli_query($conn, $query);
		$addr_id = mysqli_fetch_assoc($result);
		mysqli_close($conn);
		return $addr_id['last_insert_id()'];
	}

	/*address(*addr_no, *addr_street, *addr_brgy, *addr_city, 
	*addr_province, *addr_country, *postal_code)*/

	function updateAddress($addr){
		$conn = getConnection();
		$query = "UPDATE address SET ".
		"addr_no ='".$addr->addr_no."',".
		"addr_street='".$addr->addr_st."',".
		"addr_brgy ='".$addr->addr_brgy."',".
		"addr_city ='".$addr->addr_city."',".
		"addr_province ='".$addr->addr_prov."',".
		"addr_country ='".$addr->addr_count."',".
		"postal_code ='".$addr->zip_code."' ".
		"WHERE addr_id='".$addr->addr_id."'";
		mysqli_query($conn, $query);
		mysqli_close($conn);
	}

	function deleteAddress($addr_id){
		$conn = getConnection();
		$query = "DELETE FROM address WHERE addr_id='".$addr_id."'";
		mysqli_query($conn, $query);
		mysqli_close($conn);
	}

	function addProperty($prop){
		$conn = getConnection();
		$query = "INSERT INTO property (user_id, property_type_code, addr_id, title, cost, transac_code, rent_intv_code, cover_pic_index, feat, yrsToPay) VALUES (".
		"'".$prop->user_id."',".
		"'".$prop->prop_type."',".
		"'".$prop->addr_id."',".
		"'".$prop->prop_title."',".
		"'".$prop->prop_cost."',".
		"'".$prop->prop_adType."',".
		"'".$prop->prop_rentIntv."',".
		"'".$prop->cover_pic_index."',".
		"'".$prop->prop_feat."',".
		"'".$prop->prop_yrsToPay."')";
		mysqli_query($conn, $query);
		
		$query = "SELECT last_insert_id()";
		$result = mysqli_query($conn, $query);
		$prop_id = mysqli_fetch_assoc($result);
		mysqli_close($conn);
		return $prop_id['last_insert_id()'];
	}

	function updateProperty($prop){
		$conn = getConnection();
		$query = "UPDATE property SET ".
		"property_type_code ='".$prop->prop_type."',".	
		"title ='".$prop->prop_title."',".	
		"cost ='".$prop->prop_cost."',".
		"transac_code ='".$prop->prop_adType."',".
		"cover_pic_index ='".$prop->cover_pic_index."',".
		"feat ='".$prop->prop_feat."' ".
		"WHERE property_key='".$prop->prop_id."'";
		mysqli_query($conn, $query);

		switch($prop->prop_adType){
			case 'B':
				$query = "UPDATE property SET ".
				"yrsToPay ='".$prop->prop_yrsToPay."' ".
				"WHERE property_key ='".$prop->prop_id."'";
				break;
			case 'C':
				$query = "UPDATE property SET ".
				"rent_intv_code ='".$prop->prop_rentIntv."' ".
				"WHERE property_key ='".$prop->prop_id."'";
				break;
		}
		mysqli_query($conn, $query);
		mysqli_close($conn);
	}

	function deleteProperty($prop_id){
		$conn = getConnection();
		$query = "DELETE FROM property WHERE property_key ='".$prop_id."'";
		mysqli_query($conn, $query);
		mysqli_close($conn);
	}

	function getPropertyListByUser($user_id){
		$conn = getConnection();
		$query = "SELECT * FROM property WHERE user_id = '".$user_id."'";
		$result = mysqli_query($conn, $query);

		$count = mysqli_num_rows($result);
		$propArray = array();
		for($x=0; $x < $count; $x++){
			$row = mysqli_fetch_assoc($result);
			$temp = new Property();
			$temp->prop_id = $row['property_key'];
			$temp->user_id = $row['user_id'];
			$temp->prop_typeCode = $row['property_type_code'];
			$temp->addr_id = $row['addr_id'];
			$temp->prop_title = $row['title'];
			$temp->prop_cost = $row['cost'];
			$temp->cover_pic_index = $row['cover_pic_index'];
			$temp->prop_adType = $row['transac_code'];
			$temp->prop_feat = $row['feat'];		
			$temp->prop_acquired = $row['is_acquired'];
			
			$temp->prop_yrsToPay = 0;
			$temp->prop_rentIntv = '';

			switch($temp->prop_adType){
				case 'B':
					$temp->prop_yrsToPay = $row['yrsToPay'];
					break;
				case 'C':
					$temp->prop_rentIntv = $row['rent_intv_code'];
					break;
			}
			$propArray[$x] = $temp;
		}
		mysqli_close($conn);
		return $propArray;
	}

	function getProperty($prop_id){
		$conn = getConnection();
		$query = "SELECT * FROM property WHERE property_key = '".$prop_id."'";
		$result = mysqli_query($conn, $query);

		$count = mysqli_num_rows($result);
		$temp = new Property();
		for($x=0; $x < $count; $x++){
			$row = mysqli_fetch_assoc($result);
			$temp->prop_id = $row['property_key'];
			$temp->user_id = $row['user_id'];
			$temp->prop_type = $row['property_type_code'];
			$temp->addr_id = $row['addr_id'];
			$temp->prop_title = $row['title'];
			$temp->prop_cost = $row['cost'];
			$temp->cover_pic_index = $row['cover_pic_index'];
			$temp->prop_adType = $row['transac_code'];
			$temp->prop_feat = $row['feat'];		
			$temp->prop_acquired = $row['is_acquired'];

			$temp->prop_yrsToPay = 0;
			$temp->prop_rentIntv = '';

			switch($temp->prop_adType){
				case 'B':
					$temp->prop_yrsToPay = $row['yrsToPay'];
					break;
				case 'C':
					$temp->prop_rentIntv = $row['rent_intv_code'];
					break;
			}
		}
		mysqli_close($conn);
		return $temp;
	}

	function getAllProperty(){
		$conn = getConnection();
		$query = "SELECT * FROM property";
		$result = mysqli_query($conn, $query);

		$count = mysqli_num_rows($result);
		$list = array();
		for($x=0; $x < $count; $x++){
			$row = mysqli_fetch_assoc($result);
			$temp = new Property();
			$temp->prop_id = $row['property_key'];
			$temp->user_id = $row['user_id'];
			$temp->prop_type = $row['property_type_code'];
			$temp->addr_id = $row['addr_id'];
			$temp->prop_title = $row['title'];
			$temp->prop_cost = $row['cost'];
			$temp->cover_pic_index = $row['cover_pic_index'];
			$temp->prop_adType = $row['transac_code'];
			$temp->prop_feat = $row['feat'];		
			$temp->prop_acquired = $row['is_acquired'];
			
			$temp->prop_yrsToPay = 0;
			$temp->prop_rentIntv = '';

			switch($temp->prop_adType){
				case 'B':
					$temp->prop_yrsToPay = $row['yrsToPay'];
					break;
				case 'C':
					$temp->prop_rentIntv = $row['rent_intv_code'];
					break;
			}
			array_push($list, $temp);
		}
		mysqli_close($conn);
		return $list;
	}

	function getPropType(){
		$conn = getConnection();
		$query = "SELECT * FROM property_type";
		$result = mysqli_query($conn, $query);

		$count = mysqli_num_rows($result);
		if($count > 0){
			$list = array();
			for($x = 0; $x < $count; $x++){
				$row = mysqli_fetch_assoc($result);
				$temp = new PropertyType();
				$temp->prop_typeCode = $row['property_type_code'];
				$temp->desc = $row['description'];
				$list[$x] = $temp;
			}
			return $list;
		}
		mysqli_close($conn);
		return null;
	}

	function getTransacType(){
		$conn = getConnection();
		$query = "SELECT * FROM transac_type";
		$result = mysqli_query($conn, $query);

		$count = mysqli_num_rows($result);
		if($count > 0){
			$list = array();
			for($x = 0; $x < $count; $x++){
				$row = mysqli_fetch_assoc($result);
				$temp = new TransacType();
				$temp->transac_code = $row['transac_code'];
				$temp->desc = $row['transac_desc'];
				$list[$x] = $temp;
			}
		}

		mysqli_close($conn);
		return $list;
	}

	function getTransacTypeDesc($adTypeCode){
		$conn = getConnection();
		$query = "SELECT transac_desc FROM transac_type WHERE transac_code = '".$adTypeCode."'";
		$result = mysqli_query($conn, $query);

		$count = mysqli_num_rows($result);
		$row = mysqli_fetch_assoc($result);
		mysqli_close($conn);
		if($count > 0){
			return $row['transac_desc'];
		}
		return 'n/a';
	}

	function getRentIntv(){
		$conn = getConnection();
		$query = "SELECT * FROM rent_interval";
		$result = mysqli_query($conn, $query);

		$count = mysqli_num_rows($result);
		if($count > 0){
			$list = array();
			for($x=0; $x < $count; $x++) {
				$row = mysqli_fetch_assoc($result);
				$temp = new RentIntv();
				$temp->rent_intv_code = $row['rent_intv_code'];
				$temp->desc = $row['intv_desc'];
				$list[$x] = $temp;
			}
		}
		mysqli_close($conn);
		return $list;
	}

	function getRentIntvDesc($rent_intv_code){
		$conn = getConnection();
		$query = "SELECT intv_desc FROM rent_interval WHERE rent_intv_code = '".$rent_intv_code."'";
		$result = mysqli_query($conn, $query);

		$row = mysqli_fetch_assoc($result);
		mysqli_close($conn);
		if(count($row) > 0){
			return $row['intv_desc'];
		}
		return 'n/a';
	}

	function isLogin(){
		$flag = false;
			if(isset($_SESSION['user_id']) && !empty($_SESSION['user_id']))
				$flag = true;
		return $flag;
	}
?>