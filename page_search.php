<?php
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	include_once 'widget.php';
	init_ajax();
	
	$_SESSION['searchIds'] = array();
	$_SESSION['searchItems'] = array();
	$propList = getAllProperty();
	if(isset($_GET['searchTag'])){
		$searchTag = explode(" ", $_GET['searchTag']);
		foreach($propList as $prop){
			foreach ($searchTag as $tag) {
				if(strchr(strtolower($prop->prop_title), strtolower($tag)) != ''){
					$isExist = false;
					foreach ($_SESSION['searchIds'] as $idItem) {
						if($idItem == $prop->prop_id){
							$isExist = true;
							break;
						}
					}
					if(!$isExist){
						array_push($_SESSION['searchIds'], $prop->prop_id);
						array_push($_SESSION['searchItems'], $prop);
					}
				}
			}
		}
	}
?>

<html>
	<head>
		<title>Urban - Search Results</title>	
	</head>
	<body>
		<?php
			navBar();
			echo '<br><br>';
			groupV2("Search Result of ".$_GET['searchTag'], $_SESSION['searchItems']);
			footer();
			unset($_SESSION['searchIds']);
			unset($_SESSION['searchItems']);
		?>
	</body>
</html>