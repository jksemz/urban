<?php
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	include_once 'widget.php';
	init_ajax();

	$editable = false;
	$prop_id = $_GET['prop_id'];
	$prop = getProperty($prop_id);
	$addr = getAddress($prop->addr_id);
	$addr_parag = 
		$addr->addr_no." ".
		$addr->addr_st." ,".
		$addr->addr_brgy." ".
		$addr->addr_city." ".
		$addr->addr_prov." ".
		$addr->addr_count." ".
		$addr->zip_code;
		
	if(isLogin()){
		if($prop->user_id == $_SESSION['user_id'])
			$editable = true;
	}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>Urban - <?php echo $prop->prop_title?></title>
	</head>

	<body>
		<?php
			navBar();
			if($editable)
				editPropModal($prop->prop_id);
		?>
		<div class='w3-card-4 w3-green w3-animate-bottom' style='margin-top:40px;margin-left: 120px; margin-right: 120px; padding-top:20px;padding-left:20px;padding-right:20px;'>
			<div>
				<div style="display: inline-block;">
					<h1 style='margin-bottom: -15px'><?php echo $prop->prop_title ?></h1>
					<?php 
						switch($prop->prop_type){
							case 'R':
								echo 'RESIDENTIAL';
								break;
							case 'C':
								echo 'COMMERCIAL';
								break;
						}
					?>
				</div>
				<?php
					if($editable){?>
						<button class='w3-button w3-white w3-text-green' style="float:right;" onclick='document.getElementById("editPropModal").style.display="block"'>EDIT</button><?php } ?>
			</div>
			<p style='margin-bottom: -5px'>Located at:</p>
			<?php echo $addr_parag ?>
			<?php
				echo '<br><br>'
				?><div style="display:inline-block; white-space:nowrap"><?php
				echo '<div style="display:inline-block; white-space:nowrap">';
				filmstrip(getPropPicPath($prop_id));
				echo '</div>';
			?>
				<div style='margin-top:0px;  ;margin-left:10px; float:right;'>
					<div class='w3-card-4' style='background-color:#255628; text-align: center;'>
						<?php
							switch($prop->prop_adType){
								case 'A':
									echo 'PHP '.$prop->prop_cost;
									echo '<br>';
									echo getTransacTypeDesc($prop->prop_adType);
									break;
								case 'B':
									echo 'PHP '.round($prop->prop_cost / ($prop->prop_yrsToPay * 12));
									echo ' PER MONTH <br>';
									echo getTransacTypeDesc($prop->prop_adType).' ('.$prop->prop_yrsToPay.' YRS TO PAY)';
									break;
								case 'C':
									echo 'PHP '.$prop->prop_cost;
									echo ' '.getRentIntvDesc($prop->prop_rentIntv).'<br>';
									echo getTransacTypeDesc($prop->prop_adType);
									break;
							}
						?>
					</div>
					<div class='w3-animate-bottom' style="margin-top:10px;width: 550px; height: 308px">
						<div class='w3-card-2 w3-white' style='width: 260px; height: 308px;padding:16px; display:inline-block; vertical-align: top;'>
							<a href='page_profile.php?user_id=<?php echo $prop->user_id ?>' style='text-decoration: none'>
							<div>
								<p style="text-align: center" class='w3-text-green'>Posted by:</p>
								<div class='w3-card-2' style='width:64px; height:64px; border:5px #5bcc62 solid;display: block ;margin-left: auto; margin-right: auto;  border-radius: 50%; background-size: cover;background-position: center; background-image: url("<?php echo getProfPicPath($prop->user_id)?>")'></div>
								<h4 style="text-align: center"><?php echo getUsername($prop->user_id)?></h4>
								<?php $contact = getUserContact($prop->user_id) ?>
								<p style='margin-bottom: -5px'>Email:</p>
								<?php echo $contact->email ?>
								<p style='margin-bottom: -5px'>Phone:</p>
								<?php echo $contact->contact_no ?>
							</div>
							</a>
						</div>
						<div class='w3-card-4 w3-white w3-animate-bottom' style="display:inline-block; width: 285px; height: 308px">
							<h4 class='w3-text-green' style='margin-left: 10px'>Features</h4>
							<div style='height: 250px;overflow:scroll'>
								<p style='margin-left:10px'><?php echo nl2br($prop->prop_feat); ?></p>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</body>
</html>