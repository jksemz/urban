<?php
 	include 'ui_core.php';
 	
 	function group($header, $list){ ?>
 		<div class='w3-card-4 w3-grey' style='padding :15px;margin-left:50px;margin-right:50px'>
 		<h4 style='margin-left:5px' class='w3-text-white'><?php echo $header ?></h4>
 <?php
 		$count = count($list);
 		for($x = 0; $x < $count; $x++){
 			$temp = $list[$x];
 			tile($list[$x]);
 		}
 		echo "</div>";
 	}
?>

<?php
 	function tile($prop){ 
 		$cost_label = 'n/a';
 		$addr = getAddress($prop->addr_id);
 		switch($prop->prop_adType){
 			case 'A':
 				$cost_label = 'PHP '.shortenCost($prop->prop_cost);
 				break;
 			case 'B':
 				$cost_label = 'PHP '.shortenCost(round($prop->prop_cost / ($prop->prop_yrsToPay * 12)))." / PER MONTH <br> ".$prop->prop_yrsToPay." YRS TO PAY";
 				break;
 			case 'C':
 				$cost_label = 'PHP '.shortenCost($prop->prop_cost)." / ".getRentIntvDesc($prop->prop_rentIntv);
 				break;
 		}
 ?>
 		<a href="page_property.php?prop_id=<?php echo $prop->prop_id ?>">
 		<div class='w3-card-4' style='margin:5px;display:inline-block;background-image: url(<?php echo getCoverPropPath($prop->prop_id, $prop->cover_pic_index) ?>); background-position: center; background-size: 150%; background-repeat: no-repeat;'>
 			<table width="250px" border = '0' style='margin:-2px'>
 				<tr>
 					<td style='color:white;background: linear-gradient(rgba(0,0,0,.9), rgba(0,0,0,0));'>
 						<div style='margin-left: 5px;margin-top: 5px;font-size: 10pt'>
 						<?php echo $prop->prop_title ?> <br>
 						by <a href='page_profile.php?user_id=<?php echo $prop->user_id ?>'><?php echo getUsername($prop->user_id)?><br></a>
 						<?php echo $addr->addr_city.", ".$addr->addr_prov; ?>
 						</div>
 					</td>
 				</tr>
 				<tr height ='110px'>
 				</tr>
 				<tr height='45px'>
 					<td class='w3-card-4 w3-green' style='color:white'>
 						<center>
 						<p style='font-size: 10pt'><?php echo $cost_label ?></p>
 						</center>
 					</td>
 				</tr>
 			</table>
 		</div>
 		</a>
<?php
 	} ?>

<?php
	function groupV2($header, $list){ 
		$Rlist = array();
		$Clist = array();

		foreach ($list as $item) {
			switch($item->prop_type){
				case 'R':
					array_push($Rlist, $item);
					break;
				case 'C':
					array_push($Clist, $item);
					break;
			}
		}
?>
		<div class='w3-card-4' style='padding :15px;margin-left:50px;margin-right:50px; background-color: rgba(1,1,1,.4);'>
			<?php
				if($header != '')
					echo '<h4 class="w3-text-white">"'.$header.'"</h4>';
			?>
			<div class='w3-bar w3-green w3-card-2'>
				<button onclick='changeTab("Rlist")' id='Rbtn' class='w3-button w3-half w3-border'>RESIDENTIAL</button>
				<button onclick='changeTab("Clist")' id='Cbtn' class='w3-button w3-half w3-border'>COMMERICAL</button>
			</div>
			<div id='Rlist' class='w3-animate-bottom' style='height: 100%'>
				<center>
					<h4 class="w3-text-white">RESIDENTIAL</h4>
				</center>
			<?php
				if(count($Rlist) > 3)
					echo '<center>';
				foreach ($Rlist as $item) {
						tile($item);
				}
				if(count($Rlist) > 3)
					echo '</center>';
			?>
			</div>
			<div id='Clist' class='w3-animate-bottom' style='height: 100%'>
				<center>
					<h4 class="w3-text-white">COMMERICAL</h4>
				</center>
			<?php
				if(count($Clist) > 3)
					echo '<center>';
				foreach ($Clist as $item) {
						tile($item);
				}
				if(count($Clist) > 3)
					echo '</center>';
			?>
			</div>
		</div>
		<script>
			document.getElementById('Rbtn').innerHTML = 'RESIDENTIAL (<?php echo count($Rlist) ?>)';
			document.getElementById('Cbtn').innerHTML = 'COMMERICAL (<?php echo count($Clist) ?>)';
			changeTab('Rlist');
			function changeTab(index){
				document.getElementById('Rlist').style.display = 'none';
				document.getElementById('Clist').style.display = 'none';
				document.getElementById(index).style.display = 'block';
			}
		</script>
<?php
	} ?>
	
<?php
	function filmstrip($pic_arr){
		$count = 0;
		echo '<div>';
		for($x=0; $x < count($pic_arr); $x++) { ?>
			<img src='<?php echo $pic_arr[$x] ?>' width='450px' height='360px' id='<?php echo $x."pic" ?>' class='w3-animate-opacity w3-card-4' style='display: inline;'>
		<script>document.getElementById("<?php echo $x.'pic' ?>").style.display="none"</script>
<?php	} ?>
		<div>
		<div style='height: 10px'></div>
		<center>
			<button class='w3-button w3-text-green w3-white' onclick="prvPic()">Prev</button>
			<button class='w3-button w3-text-green w3-white' onclick="nxtPic()">Next</button>
			<br>
			<span id='indc'></span>
		</center>
		</div>
		</div>

		<script>
			var index = 0;
			var maxIndex = <?php echo count($pic_arr) ?>;
			change(index);
			
			function nxtPic(){
				if(index < maxIndex-1){
					index++;
					change(index);
				}
			}

			function prvPic(){
				if(index != 0){
					index--;
					change(index);	
				}
			}

			function change(ind){
				for(var x = 0; x < maxIndex ; x++)
					document.getElementById(x+"pic").style.display = 'none';
				document.getElementById(ind+"pic").style.display = 'block';
				document.getElementById('indc').innerHTML = (ind + 1) + "/" + maxIndex;
			}
		</script>
<?php
 	} ?>